#include <iostream>
#include <easy/jit.h>

int simple_mul(int x, int y){
	return x * y;
}

class Test
{
private:
	int x;

public:
	Test();

	int getResult(int y);

};

Test::Test() : x(2) {}

int Test::getResult(int y){
	auto mult_jit = easy::jit(simple_mul, x, std::placeholders::_1);
	return mult_jit(y);
}

int main(int argc, char** argv){

	Test obj;
	std::cout << "result = " << obj.getResult(argc) << "\n";
}