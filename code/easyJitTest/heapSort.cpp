#include <easy/jit.h>
#include <easy/code_cache.h>
//code de base repris de : https://www.geeksforgeeks.org/cpp-program-for-heap-sort/
void heapify(int arr[], int n, int i) 
{ 
    int largest = i; // Initialize largest as root 
    int l = 2 * i + 1; // left = 2*i + 1 
    int r = 2 * i + 2; // right = 2*i + 2 
  
    // If left child is larger than root 
    if (l < n && arr[l] > arr[largest]) 
        largest = l; 
  
    // If right child is larger than largest so far 
    if (r < n && arr[r] > arr[largest]) 
        largest = r; 
  
    // If largest is not root 
    if (largest != i) { 
        std::swap(arr[i], arr[largest]); 
  
        // Recursively heapify the affected sub-tree 
        heapify(arr, n, largest); 
    } 
} 
  
// main function to do heap sort 
void heapSort(int arr[], int n)
{ 
    // Build heap (rearrange array) 
    for (int i = n / 2 - 1; i >= 0; i--) 
        heapify(arr, n, i); 
  
    // One by one extract an element from heap 
    for (int i = n - 1; i >= 0; i--) { 
        // Move current root to end 
        std::swap(arr[0], arr[i]); 
  
        // call max heapify on the reduced heap 
        heapify(arr, i, 0); 
    } 
}

void jitedHeapify(int arr[], int n){
    using namespace std::placeholders;
    auto my_heapify = easy::jit(heapify, _1, _2, _3);
    
    for (int i = n / 2 - 1; i >= 0; i--){
        my_heapify(arr, n, i);
    }
    for (int i = n - 1; i >= 0; i--) { 
        // Move current root to end 
        std::swap(arr[0], arr[i]); 
  
        // call max heapify on the reduced heap 
        my_heapify(arr, i, 0); 
    }
}

void jitedCachedHeapify(int arr[], int n){
    using namespace std::placeholders;
    static easy::Cache<> cache;
    
    auto const &heapify_opt = cache.jit(heapify, _1,_2,_3);
    for (int i = n / 2 - 1; i >= 0; i--){
        heapify_opt(arr, n, i);
    }
    for (int i = n - 1; i >= 0; i--) { 
        // Move current root to end 
        std::swap(arr[0], arr[i]); 
  
        // call max heapify on the reduced heap 
        heapify_opt(arr, i, 0); 
    }
}

void jitedHeapSort(int arr[], int n){
    using namespace std::placeholders;
    auto JHS = easy::jit(heapSort, _1, _2);
    
    JHS(arr, n);
}

void jitedCachedHeapSort(int arr[], int n){
    using namespace std::placeholders;
    static easy::Cache<> cache;
    
    auto const &JCHS = cache.jit(heapSort, _1, _2);
    JCHS(arr, n);
}
