#include <iostream>
#include <numeric>
#include <vector>
#include <algorithm>

#include "heapSort.h"
void printArray(int arr[], int n) 
{ 
    for (int i = 0; i < n; ++i) 
        std::cout << arr[i] << " "; 
    std::cout << "\n"; 
} 

int value[] = {10000, 100000, 1000000, 5000000};
int main(int argc, char** argv) {
        std::vector<int> vec(value[3]);

        std::iota(vec.begin(), vec.end(), 0);
        std::random_shuffle(vec.begin(), vec.end());
        std::cout << "begin heapSort\n";
        heapSort(vec.data(), value[3]);
        
        std::cout << "array sorted " << vec.at(0) << "\n";
        std::random_shuffle(vec.begin(), vec.end());
        std::cout << "begin jited heapSort\n";
        
        jitedHeapSort(vec.data(), value[3]);
        std::cout << "array sorted " << vec.at(0) << "\n";
        
        
        std::random_shuffle(vec.begin(), vec.end());
        std::cout << "begin jited heapify\n";
        jitedHeapify(vec.data(), value[3]);

        
        std::cout << "array sorted " << vec.at(0) << "\n";
        

}
