#include <benchmark/benchmark.h>
#include <easy/jit.h>
#include <numeric>
#include "heapSort.h"

static void BM_originalSort(benchmark::State& state){
    int n = state.range(0);
    
    std::vector<int> vec(n);
    std::iota(vec.begin(), vec.end(), 0);
    std::random_shuffle(vec.begin(), vec.end());
    
    benchmark::ClobberMemory();
    
    for(auto _ : state){
    heapSort(vec.data(), n);
    }
}
BENCHMARK(BM_originalSort)->Range(10000, 4000000)->Unit(benchmark::kMillisecond);

static void BM_jitedHeapify(benchmark::State& state){
    int n = state.range(0);
    std::vector<int> vec(n);
    std::iota(vec.begin(), vec.end(), 0);
    std::random_shuffle(vec.begin(), vec.end());
    
    benchmark::ClobberMemory();
    
    for(auto _ : state){
        jitedHeapify(vec.data(), n);
    }
}
BENCHMARK(BM_jitedHeapify)->Range(10000, 4000000)->Unit(benchmark::kMillisecond);

static void BM_jitedHeapSort(benchmark::State& state){
    using namespace std::placeholders;
    int n = state.range(0);
    
    std::vector<int> vec(n);
    std::iota(vec.begin(), vec.end(), 0);
    std::random_shuffle(vec.begin(), vec.end());
    
    benchmark::ClobberMemory();
    
    //auto jitedHeapSort = easy::jit(heapSort, _1, _2);
    for(auto _ : state){
        jitedHeapSort(vec.data(),n);
        
    }
}
BENCHMARK(BM_jitedHeapSort)->Range(10000, 4000000)->Unit(benchmark::kMillisecond);


static void BM_jitedCachedHeapify(benchmark::State& state){
    int n = state.range(0);
    std::vector<int> vec(n);
    std::iota(vec.begin(), vec.end(), 0);
    std::random_shuffle(vec.begin(), vec.end());
    
    benchmark::ClobberMemory();
    
    for(auto _ : state){
        jitedCachedHeapify(vec.data(), n);
    }
}
BENCHMARK(BM_jitedCachedHeapify)->Range(10000, 4000000)->Unit(benchmark::kMillisecond);

static void BM_jitedCachedHeapSort(benchmark::State& state){
    using namespace std::placeholders;
    int n = state.range(0);
    
    std::vector<int> vec(n);
    std::iota(vec.begin(), vec.end(), 0);
    std::random_shuffle(vec.begin(), vec.end());
    
    benchmark::ClobberMemory();
    
    //auto jitedHeapSort = easy::jit(heapSort, _1, _2);
    for(auto _ : state){
        jitedCachedHeapSort(vec.data(),n);
        
    }
}
BENCHMARK(BM_jitedCachedHeapSort)->Range(10000, 4000000)->Unit(benchmark::kMillisecond);

BENCHMARK_MAIN();
