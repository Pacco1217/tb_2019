#include <stdio.h>
template <int val>
[[clang::jit]] int globalGetResult(int x){
    return val * x;
}

class Test
{
private:
    int x;

public:
    Test();

    int getResult(int val){
        return globalGetResult<val>(x);
    }
};

Test::Test() : x(2){}


int main(int argc, char ** argv) {
    int result;
    Test obj;
    result = obj.getResult(argc);
    printf("%d\n", result);
    return 0;
}
