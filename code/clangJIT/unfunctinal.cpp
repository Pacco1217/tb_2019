#include <iostream>
 
class Test
{
private:
	int x;

public:
	Test();

	template<int val>
	int getResult();

};

Test::Test() : x(2) {}

template <int val>
[[clang::jit]] int Test::getResult(){
	return val * x;
}

int main(){

	Test obj;

	std::cout << "result = " << obj.getResult<5>() << "\n";

}