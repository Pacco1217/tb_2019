#include <stdio.h>
template <int val>
[[clang::jit]] int simple_mul(int x){
	return x * val;
}

int main(int argc, char ** argv){
	int result = simple_mul<argc>(5);
	printf("%d\n", result);
}
