/* -*- c++ -*- */
/* 
 * Copyright 2019 gr-addJIT author.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "add_const_ii_impl.h"
#ifdef EASY_JIT
#include <easy/jit.h>
#include <easy/code_cache.h>
#endif

namespace gr {
  namespace addJIT {

    #ifdef EASY_JIT
      int
      simple_add(int a, int b){
        return a + b;
      }
    #endif

    #ifdef CLANG_JIT
      template <int c>
      [[clang::jit]] int simple_add(int a){
        return a + c;
      }
    #endif

    add_const_ii::sptr
    add_const_ii::make(int k)
    {
      return gnuradio::get_initial_sptr
        (new add_const_ii_impl(k));
    }

    /*
     * The private constructor
     */
    add_const_ii_impl::add_const_ii_impl(int k)
      : gr::sync_block("add_const_ii",
              gr::io_signature::make(1, 1, sizeof(int)),
              gr::io_signature::make(1, 1, sizeof(int))),
      d_k(k)
    {}

    /*
     * Our virtual destructor.
     */
    add_const_ii_impl::~add_const_ii_impl()
    {
    }

    int
    add_const_ii_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const int *in = (const int *) input_items[0];
      int *out = (int *) output_items[0];

      int size = noutput_items;
      #ifdef EASY_JIT
        static easy::Cache<> cache;

        auto const &add_cache_opt = cache.jit(simple_add, std::placeholders::_1, d_k);

        while(size-- > 0){
          *out++ = add_cache_opt(*in++);
        }
      #endif
      #ifdef CLANG_JIT
        while(size-- > 0){
          *out++ = simple_add<d_k>(*in++);
        }
      #endif

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace addJIT */
} /* namespace gr */

