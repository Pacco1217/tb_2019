/* -*- c++ -*- */

#define ADDJIT_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "addJIT_swig_doc.i"

%{
#include "addJIT/add_const_ii.h"
%}


%include "addJIT/add_const_ii.h"
GR_SWIG_BLOCK_MAGIC2(addJIT, add_const_ii);
