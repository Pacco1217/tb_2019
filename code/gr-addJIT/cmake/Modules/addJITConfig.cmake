INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_ADDJIT addJIT)

FIND_PATH(
    ADDJIT_INCLUDE_DIRS
    NAMES addJIT/api.h
    HINTS $ENV{ADDJIT_DIR}/include
        ${PC_ADDJIT_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    ADDJIT_LIBRARIES
    NAMES gnuradio-addJIT
    HINTS $ENV{ADDJIT_DIR}/lib
        ${PC_ADDJIT_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(ADDJIT DEFAULT_MSG ADDJIT_LIBRARIES ADDJIT_INCLUDE_DIRS)
MARK_AS_ADVANCED(ADDJIT_LIBRARIES ADDJIT_INCLUDE_DIRS)

