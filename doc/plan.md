# Plan du rapport
1. Introduction
2. Compilateur
    1. Architecture
    2. Front-end
    3. middle-end
    4. back-end
    3. Just in time compiler (API LLVM ?)
    5. Présentation LLVM (?)
3. State of the art (Comment et où est utilisée la technologie)
4. Présentation easy::jit (objectif du projet, utilisation, fonctionnement interne)
5. Première prise en main d'easy::jit
    1. code exemple fourni
    2. Exemple personnel
    3. Analyse de performance simple (flameGraph)
