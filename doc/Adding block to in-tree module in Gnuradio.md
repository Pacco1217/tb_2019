# Adding block to in-tree module in Gnuradio

1. create include file and add new file in include/gnuradio/<module_name>/CMakeLists.txt
2. create .h and .cc file for the implementation in lib/ and add file in lib/CMakeLists.txt
3. create xml file in grc/ and add name in grc/<module_name>_block_tree.xml
4. edit grc/CMakeLists.txt
5. adding info in swig/<module_name>_swig.i
6. cmake ...
7. sudo make install
8. enjoy !